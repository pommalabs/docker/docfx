# DocFX Docker image

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]

Docker image with [DocFX][docfx-website], based on Debian Linux.
Image also contains [Playwright][playwright-website] and its dependencies:
Playwright is a tool used by DocFX in order to generate PDF files.

## Table of Contents

- [DocFX Docker image](#docfx-docker-image)
  - [Table of Contents](#table-of-contents)
  - [Tags](#tags)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
    - [Building Docker image](#building-docker-image)
  - [License](#license)

## Tags

Following tags are available:

| Tag      | WORKDIR    | Base image                                                | DocFX  |
|----------|------------|-----------------------------------------------------------|--------|
| `latest` | `/opt/prj` | `container-registry.pommalabs.xyz/pommalabs/dotnet:9-sdk` | Latest |

Each tag can be downloaded with following command, just replace `latest` with desired tag:

```bash
docker pull container-registry.pommalabs.xyz/pommalabs/docfx:latest
```

Tags are rebuilt every week by a job scheduled on GitLab CI platform.

## Usage

Command line usage is pretty simple:

```bash
cd /path/to/project
docker run -it --rm -v ${pwd}:/opt/prj container-registry.pommalabs.xyz/pommalabs/docfx DOCFX_COMMAND_LINE_ARGUMENTS
```

For example, building a DocFX-based documentation stored in `docs`
directory can be achieved with following command:

```bash
cd /path/to/project
docker run -it --rm -v ${pwd}:/opt/prj container-registry.pommalabs.xyz/pommalabs/docfx docs/docfx.json
```

DocFX can also be easily used as a step in GitLab CI:

```yaml
pages:
  stage: deploy
  image:
    name: container-registry.pommalabs.xyz/pommalabs/docfx
    entrypoint: [""]
  script:
    - docfx docs/docfx.json
    - mv docs/_site public
  artifacts:
    paths:
      - public
```

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.
I replaced the __Install__ section with __Tags__, since I thought that it made no sense
to "install" an helper Docker image.

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `docfx`).

## License

MIT © 2022-2024 [PommaLabs Team and Contributors][pommalabs-website]

[docfx-website]: https://dotnet.github.io/docfx/
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/docker/docfx/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/docker/docfx/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[playwright-website]: https://playwright.dev/
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/docker/docfx/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
